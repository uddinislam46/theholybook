<?php
session_start();
			include 'connection.php';
if (!isset($_SESSION['email'])){
	echo('<a href="login.php">Login here:</a>');
}else{
		echo $_SESSION['email'].' | ';
		echo "<a href='logout.php'>Logout</a>";

}
?>	
<!-- HTML starts -->
<html>
<!-- HTML Head starts -->
<head>
	<!-- Q icon on URL (favicon) -->
	<link rel="shortcut icon" href="https://i2.wp.com/alquran.pro/wp-content/uploads/2018/01/cropped-alquran.pro-logo-v2-1.png?w=512&ssl=1" type="image/x-icon"/>
	<!-- Title on URL -->
	<title>The Quran Mubeen </title>
	<!-- Add your internal CSS here for styling -->
	<style>
		.textarea {
			display: inline-block;
			border: solid 1px #000;
			min-height: 0px;
			width: 100%;
		}
		.text {
			display: inline-block;
			border: solid 1px #000;
			min-height: 10px;
			width: 1000px;
		}


		
		body.rtl {
			direction: rtl;
		}
		input {
			display: block;
			direction: rtl;
		}

        * {
            font-size: 20px;
            font-family: "Times New Roman", Times, serif;
        }

	</style>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<head>

<!-- HTML body starts -->
<body class="rtl">
		<fieldset style="text-align:right;">
		<legend>آیات محفوظ کرنے کا فارم </legend>
			<form method="POST" action="suratdetails.php">
				
		<table>
			<tr>
				<td>سورة نمبر</td><td><input type="text" name="SURAT_ID"> </td>
				</tr>
				<tr>
				<td>أيه نمبر</td><td><input type="text" name="AYAT_NO"> </td>
				</tr>
				<tr>
				<td>آيات</td>
				<td>
					<input type="hidden" name="AYAH_TEXT" value=""/>
					<div   class="text" contentEditable="true"></div>
				</td>
				</tr>
				<tr>
				<td>ترجمہ جمیل نوری</td>
				<td>
					<input type="hidden" name="TARJUMA_JAMEEL_NORI" value=""/>
					<div   class="text" contentEditable="true"></div>
				</td>
				</tr>
				<tr>
				<td>تفسیر جمیل نوری</td>
				<td>
					<input type="hidden" name="TAFSEER_JAMEEL_NORI" value=""/>
					<div   class="text" contentEditable="true"></div>
				</td>
				</tr>
				<tr>
				<td>فضیلت جمیل نوری</td>
				<td>
				
					<input type="hidden" name="Fazeelat_JAMEEL_NORI" value=""/>
					<div   class="text" contentEditable="true"></div>
				</td>
			</tr>
				<tr>
			
					<td></td><td><input type="submit" name="submit" value="محفوظ کریں"></td>
					<td></td><td><input type="reset" name="submit" value="منسوخ"></td>
			</tr>
			</table>
			</form>
			
			<?php	
if(isset($_POST['submit'])){ //ye line add ki he isliye k agar submit button click ho to neche wala code call ho		
			$SURAT_ID =$_POST['SURAT_ID'];
			$AYAT_NO = $_POST['AYAT_NO'];
			$AYAH_TEXT = $_POST['AYAH_TEXT'];
			$TARJUMA_JAMEEL_NORI = $_POST['TARJUMA_JAMEEL_NORI'];
			$TAFSEER_JAMEEL_NORI = $_POST['TAFSEER_JAMEEL_NORI'];
			$Fazeelat_JAMEEL_NORI = $_POST['Fazeelat_JAMEEL_NORI'];
/* 				mysqli_query($conn,"SET NAMES 'utf8'");
				mysqli_query($conn,'SET CHARACTER SET utf8');
				//mysqli_set_charset('utf8',$conn);
				
				Change character set to utf8
				mysqli_set_charset($conn,"utf8");
 */
				//Change character set to utf8
				mysqli_query($connection,"SET NAMES 'utf8'");
				mysqli_set_charset($connection,"utf8");

				
//$sql = "INSERT INTO `surat_details` (`ID`, `SURAT_ID`, `AYAT_NO`, `AYAH_TEXT`, `TARJUMA_JAMEEL_NORI`, `TAFSEER_JAMEEL_NORI`, `Fazeelat_JAMEEL_NORI`)
//VALUES ('".$_POST["SURAT_ID"]."','".$_POST["AYAT_NO"]."','".$_POST["AYAH_TEXT"]."','".$_POST["TARJUMA_JAMEEL_NORI"]."','".$_POST["TAFSEER_JAMEEL_NORI"]."','".$_POST["Fazeelat_JAMEEL_NORI"]."')";

$sql = "INSERT INTO surat_details(SURAT_ID, AYAT_NO,AYAH_TEXT,TARJUMA_JAMEEL_NORI,TAFSEER_JAMEEL_NORI,Fazeelat_JAMEEL_NORI)
 VALUES('$SURAT_ID','$AYAT_NO','$AYAH_TEXT`','$TARJUMA_JAMEEL_NORI','$TAFSEER_JAMEEL_NORI','$Fazeelat_JAMEEL_NORI' )";
//echo $sql;
 if ($connection->query($sql) === TRUE) {
echo "<script type= 'text/javascript'>alert('New record created successfully');</script>";
} else {
echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connection->error."');</script>";
}

}//end isset checking for submit button
	?>
		</fieldset>
		<fieldset>
		<legend>(EXCEL) ایکسل کے ذریعے آیات محفوظ کرنے کا فارم  </legend>
			<form action="sora-read.php" method="post" enctype="multipart/form-data">
				<input type="file" name="excelToUpload" id="excelToUpload"><br>
				<input type="submit" value="منتقل کریں " name="excel">
			</form>
		</fieldset>
		<fieldset>
		<legend>(CSV) سی. س. وی کے ذریعے آیات محفوظ کرنے کا فارم </legend>		
			<form action="csvread.php" method="post" enctype="multipart/form-data">

				<input type="file" name="csvToUpload" id="csvToUpload">
				<input type="submit" value="منتقل کریں " name="csv">
			</form>
		</fieldset>
		<fieldset>
		<legend>آیات محفوظ کرنے کا فارم </legend>		
			<form action="jason1.php" method="post" enctype="multipart/form-data">
				Json File Import Here
				<input type="file" name="jsonToUpload" id="jsonToUpload">
				<input type="submit" value="منتقل کریں" name="json">
			</form>
		</fieldset>	

	<!-- php code starts -->
	<?php
	
			// starts if body for checking connection successfull/true or not
			if ($connection == TRUE){
				$get_all_records_sql = "SELECT * FROM surat_details ";
				
				//Hey Developer, if you remove these 2 lines Arabic will not show on browser because arabic is UTF-8 and browser's /
				//html's default encoding is ISO-8859-1 or ANSI. So  don't remove these lines 
				mysqli_query($connection,"SET NAMES 'utf8'");
				mysqli_query($connection,'SET CHARACTER SET utf8');
				
				
				//getting query result
				$result = mysqli_query($connection, $get_all_records_sql);
			
				
				//starts if body for checking result records should be greater than 0 records
				if (mysqli_num_rows($result) > 0) {
				// output data of each row
	?>		
	

<?php
					while($row = mysqli_fetch_assoc($result)) { // start body of while loop ?>														 
							<fieldset style="text-align:right;">
							<legend><?php echo "سورة :".$row["SURAT_ID"]." آیت : ".$row["AYAT_NO"];?> </legend>
								<span id="input-<?php echo $row["ID"] ?>" style="display:none;">
									<form action="update.php" method="post" id="myForm-<?php echo $row["ID"];?>">

									<input type="hidden" name="id" value="<?php echo $row["ID"];?>" >									
									<input type="text" name="SURAT_ID" value="<?php echo $row["SURAT_ID"];?>" > <br>
									<input type="text" name="AYAT_NO" value="<?php echo $row["AYAT_NO"];?>" > <br>									

									<input type="hidden" name="AYAH_TEXT" value=" " id="AYAT_TEXT_<?php echo $row["ID"] ?>">							
									<div class="textarea" contentEditable="true" id="AYAT_TEXT_DIV_<?php echo $row["ID"] ?>"><?php echo $row["AYAH_TEXT"];?></div>
<h4 align="left">:Translation</h4>									
									<input type="hidden" name="TARJUMA_JAMEEL_NORI" value=" " id="TARJUMA_JAMEEL_NORI_<?php echo $row["ID"] ?>">
									<div class="textarea" contentEditable="true" id="TARJUMA_JAMEEL_NORI_DIV_<?php echo $row["ID"] ?>"><?php echo $row["TARJUMA_JAMEEL_NORI"];?></div>
<h4 align="left">:Tafseer</h4>
									<input type="hidden" name="TAFSEER_JAMEEL_NORI" value=" " id="TAFSEER_JAMEEL_NORI_<?php echo $row["ID"] ?>">									
									<div class="textarea" contentEditable="true" id="TAFSEER_JAMEEL_NORI_DIV_<?php echo $row["ID"] ?>"><?php echo $row["TAFSEER_JAMEEL_NORI"];?></div>
<h4 align="left">:Fazeelat</h4>
									<input type="hidden" name="Fazeelat_JAMEEL_NORI" value=" " id="Fazeelat_JAMEEL_NORI_<?php echo $row["ID"] ?>">									
									<div class="textarea" contentEditable="true" id="Fazeelat_JAMEEL_NORI_DIV_<?php echo $row["ID"] ?>"><?php echo $row["Fazeelat_JAMEEL_NORI"];?></div>
									
									<p align="left"> <input id="update-<?php echo $row["ID"] ?>" name="update" type="button" onclick='updateButton("edit",<?php echo $row["ID"] ?>)' value="Update" style="display:none;"></p>
									</form>
								</span>
								<span id="label-<?php echo $row["ID"] ?>" >

									 <br>

									<?php
									if($row["Fazeelat_JAMEEL_NORI"] != '')
									{  
									?>
<h4 align="left">:Fazeelat</h4>
									<?php echo $row["Fazeelat_JAMEEL_NORI"]."<br><br>"; //printing in Surah Tafseer, Fazeelat with Jameel Nori Nastaliq column of table
									}
									
									?>
                                    <?php echo $row["AYAH_TEXT"]; //printing in Arabic with Muhammadi column of table ?><br>
									<?php
										if (isset($_SESSION['email'])){
											?><script> console.log("login") </script>
											<p align="left"> <input id="edit-<?php echo $row["ID"] ?>" name="edit" type="submit" onclick='updateButton("update",<?php echo $row["ID"] ?>)' value="Edit" ></p>
											<?php
											//echo('hello............');
										}
										else 
										{		
												?><script> console.log("not login") </script><?php
												//echo('not hello......');
										}
										?>
<h4 align="left">:Translation</h4>
                                    <?php echo $row["TARJUMA_JAMEEL_NORI"]; //printing in Tarjuma with Jameel Nori Nastaliq column of table ?><br> <br>
<h4 align="left">:Tafseer</h4>
									<?php echo $row["TAFSEER_JAMEEL_NORI"]; //printing in Tafseer according to Ayat with Jameel Nori Nastaliq column of table ?><br>
								

                                    </fieldset>
								</span>								

							
<?php
					}//end body of while loop
					
 ?>
				</div>
				
<?php				
				}//end if body for checking result records should be greater than 0 records
				else {
					echo "0 results";
				}
			}// ends if body for checking connection successfull/true or not


			
?>

	
<script>
function updateButton(type,id) {
	if(type === "update"){
		document.getElementById("update-"+id).style.display = "block";		
		document.getElementById("edit-"+id).style.display = "none";
		
		document.getElementById("input-"+id).style.display = "block";
		document.getElementById("label-"+id).style.display = "none";

		
	}	
	if(type === "edit"){
		var AYAT_TEXT_DIV=document.getElementById("AYAT_TEXT_DIV_"+id).innerText;
		document.getElementById("AYAT_TEXT_"+id).value=AYAT_TEXT_DIV;		

		var TARJUMA_JAMEEL_NORI_DIV_=document.getElementById("TARJUMA_JAMEEL_NORI_DIV_"+id).innerText;
		document.getElementById("TARJUMA_JAMEEL_NORI_"+id).value=TARJUMA_JAMEEL_NORI_DIV_;		

		var Fazeelat_JAMEEL_NORI_DIV_=document.getElementById("Fazeelat_JAMEEL_NORI_DIV_"+id).innerText;
		document.getElementById("Fazeelat_JAMEEL_NORI_"+id).value=Fazeelat_JAMEEL_NORI_DIV_;		

		var TAFSEER_JAMEEL_NORI_DIV_=document.getElementById("TAFSEER_JAMEEL_NORI_DIV_"+id).innerText;
		document.getElementById("TAFSEER_JAMEEL_NORI_"+id).value=TAFSEER_JAMEEL_NORI_DIV_;		

		
		
		document.getElementById("edit-"+id).style.display = "block";
		document.getElementById("update-"+id).style.display = "none";
		
		document.getElementById("label-"+id).style.display = "block";
		document.getElementById("input-"+id).style.display = "none";
		document.getElementById("myForm-"+id).submit();
	}
}
</script>


	
</body>
<style>
	 direction: rtl;

</style>
