<?php

include 'connection.php';
?>
<html>
<!-- HTML Head starts -->
<head>
	<!-- Q icon on URL (favicon) -->
	<link rel="shortcut icon" href="https://i2.wp.com/alquran.pro/wp-content/uploads/2018/01/cropped-alquran.pro-logo-v2-1.png?w=512&ssl=1" type="image/x-icon"/>
	<!-- Title on URL -->
	<title>The Quran Mubeen </title>
	<!-- Add your internal CSS here for styling -->
	<style>
		body.rtl {
			direction: rtl;
		}

        * {
            font-size: 20px;
            font-family: "Times New Roman", Times, serif;
        }

	</style>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	
	<script >
		function resetValues(){
			document.getElementById('surat_id').value='';
			document.getElementById('ayat_no').value='';
		};
	</script>
	
<head>
 <!-- HTML body starts -->
<body >

	<form action="index.php" method="POST" id="search" style="text-align:center">
	
		<input type="submit" name="reset" value="منسوخ کریں" onclick="resetValues();">
		<input type="submit" name="search" value="تلاش کریں" >
		<input type="number" name="surat_id" placeholder="سورة نمبر " id="surat_id"  value="<?php if(isset($_POST['surat_id'])) echo $_POST['surat_id']; ?>"> 
		<input type="number" name="ayat_no" placeholder="آیت نمبر" id="ayat_no"  value="<?php if(isset($_POST['ayat_no'])) echo $_POST['ayat_no']; ?>"> 
	</form>

	<!-- php code starts -->
	<?php
				if ($connection == TRUE){
				$get_all_records_sql = "SELECT * FROM surat_details ";
				if(!empty(isset($_POST['surat_id'])) && $_POST['surat_id']!="")	$get_all_records_sql .=((strpos($get_all_records_sql, 'where')) ? ' and ' : ' where ')." surat_id='".$_POST['surat_id']."'";					
				if(!empty(isset($_POST['ayat_no'])) && $_POST['ayat_no']!="")   $get_all_records_sql .=((strpos($get_all_records_sql, 'where')) ? ' and ' : ' where ')." ayat_no='".$_POST['ayat_no']."'";
				
				mysqli_set_charset($connection,"utf8");
				mysqli_query($connection,"SET NAMES 'utf8'");
				mysqli_query($connection,'SET CHARACTER SET utf8');

				//getting query result
				$result = mysqli_query($connection, $get_all_records_sql);				
				//starts if body for checking result records should be greater than 0 records				
				if (mysqli_num_rows($result) > 0) {
	?>		
<!-- Showing records into HTML Table as table row ( using <tr> and <td> table  -->
<?php
		


					while($row = mysqli_fetch_assoc($result)) { // start body of while loop ?>														 
																<span id="label-<?php //echo $row["ID"] ?>" >

									 <br>
                                    <fieldset style="text-align:right;">
                                    <legend>
									
									<?php echo "سورة :".$row["SURAT_ID"]." آیت : ".$row["AYAT_NO"];?> </legend>
 
									<?php
									if($row["Fazeelat_JAMEEL_NORI"] != '')
									{  
									?>
										<h4 align="left">Fazeelat:</h4>
									<?php echo $row["Fazeelat_JAMEEL_NORI"]."<br><br>"; //printing in Surah Tafseer, Fazeelat with Jameel Nori Nastaliq column of table
									}
									
									?>
                                    <?php echo $row["AYAH_TEXT"]; //printing in Arabic with Muhammadi column of table ?><br>
<h4 align="left">Translation:</h4>
                                    <?php echo $row["TARJUMA_JAMEEL_NORI"]; //printing in Tarjuma with Jameel Nori Nastaliq column of table ?><br> <br>
<h4 align="left">Tafseer:</h4>
									<?php echo $row["TAFSEER_JAMEEL_NORI"]; //printing in Tafseer according to Ayat with Jameel Nori Nastaliq column of table ?><br>

                                    </fieldset>
								</span>
								
								

							
<?php
					}//end body of while loop
	}				
 ?>
				
<?php				
				}//end if body for checking result records should be greater than 0 records
				else {
					echo "0 results";
				
			}// ends if body for checking connectionection successfull/true or not


			
?>

</body>
</html>
<style>
	 direction: rtl;

</style>
